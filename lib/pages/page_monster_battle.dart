import 'package:flutter/material.dart';
import 'package:forget_monster_app/components/component_my_monster_battle_item.dart';
import 'package:forget_monster_app/components/component_oppornent_monster_battle_item.dart';
import 'package:forget_monster_app/model/monster_item.dart';

class PageMonsterBattle extends StatefulWidget {
  const PageMonsterBattle({super.key});

  @override
  State<PageMonsterBattle> createState() => _PageMonsterWaitingState();
}

class _PageMonsterWaitingState extends State<PageMonsterBattle> {
  MonsterItem monster1 = MonsterItem(1, '고북이', 'assets/gobuck.png', 70, 13, 9, 8, '물');
  MonsterItem monster2 = MonsterItem(2, '이상해띠', 'assets/isang.png', 40, 12, 7, 10, '풀');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Battle'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ComponentOtherMonsterBattleItem(
              monsterItem: monster2,
              callback: () {},
          ),
          ComponentMyMonsterBattleItem(
              monsterItem: monster1,
              callback: () {},
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:forget_monster_app/components/component_monster_item.dart';
import 'package:forget_monster_app/model/monster_item.dart';
import 'package:forget_monster_app/pages/page_monster_battle.dart';

class PageMonsterWaiting extends StatefulWidget {
  const PageMonsterWaiting({super.key});

  @override
  State<PageMonsterWaiting> createState() => _PageMonsterWaitingState();
}

class _PageMonsterWaitingState extends State<PageMonsterWaiting> {
  MonsterItem monster1 = MonsterItem(1, '고북이', 'assets/gobuck.png', 70, 13, 9, 8, '물');
  MonsterItem monster2 = MonsterItem(2, '이상해띠', 'assets/isang.png', 40, 12, 7, 10, '풀');
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('포겟몬 대기실'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ComponentMonsterItem(
                    monsterItem: monster1,
                    callback: () {},
                ),
            Center(
              child: Text('VS',
              style: TextStyle(
                fontSize: 40,
              ),
              ),
            ),
                ComponentMonsterItem(
                    monsterItem: monster2,
                    callback: () {},
                ),
              ],
            ),
          ),
          Container(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMonsterBattle()));
                },
                child: const Text('Battle Start!'),
            ),
          ),
        ],
      ),
    );
  }
}

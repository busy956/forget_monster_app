import 'package:flutter/material.dart';
import 'package:forget_monster_app/pages/page_monster_battle.dart';
import 'package:forget_monster_app/pages/page_monster_waiting.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: PageMonsterWaiting(),
    );
  }
}


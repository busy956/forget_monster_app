import 'package:flutter/material.dart';
import 'package:forget_monster_app/model/monster_item.dart';

class ComponentMonsterItem extends StatefulWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback,
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;

  @override
  State<ComponentMonsterItem> createState() => _ComponentMonsterItemState();
}

class _ComponentMonsterItemState extends State<ComponentMonsterItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          OutlinedButton(
              onPressed: () {},
              child: Text('빼기'),
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            width: 200,
            height: 200,
            child: Image.asset(widget.monsterItem.imgUrl),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: Column(
              children: [
                Text(widget.monsterItem.monsterName,
                style: TextStyle(
                  fontSize: 20,
                ),
                ),
                Text('체력 ${widget.monsterItem.monsterHp}'),
                Text('공격 ${widget.monsterItem.monsterAttack}'),
                Text('방어 ${widget.monsterItem.monsterDefence}'),
                Text('속도 ${widget.monsterItem.monsterSpeed}'),
                Text('속성 ${widget.monsterItem.monsterType}')
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

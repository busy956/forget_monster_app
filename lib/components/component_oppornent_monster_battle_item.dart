import 'package:flutter/material.dart';
import 'package:forget_monster_app/model/monster_item.dart';

class ComponentOtherMonsterBattleItem extends StatefulWidget {
  const ComponentOtherMonsterBattleItem({
    super.key,
    required this.monsterItem,
    required this.callback
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;

  @override
  State<ComponentOtherMonsterBattleItem> createState() => _ComponentMonsterBattleItemState();
}

class _ComponentMonsterBattleItemState extends State<ComponentOtherMonsterBattleItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Row(
        children: [
          Column(
            children: [
              Text(widget.monsterItem.monsterName,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text('체력 ${widget.monsterItem.monsterHp}'),
              Text('공격 ${widget.monsterItem.monsterAttack}'),
              Text('방어 ${widget.monsterItem.monsterDefence}'),
              Text('속도 ${widget.monsterItem.monsterSpeed}'),
              Text('속성 ${widget.monsterItem.monsterType}'),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width / 2,
                child: LinearProgressIndicator(
                  value: 1,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              OutlinedButton(
                onPressed: () {},
                child: Text('공격'),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.all(15),
            child: SizedBox(
              width: MediaQuery.of(context).size.width / 3,
              height: MediaQuery.of(context).size.height / 3,
              child: Image.asset(
                widget.monsterItem.imgUrl,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:forget_monster_app/model/monster_item.dart';

class MonsterListResult {
  String msg;
  num code;
  List<MonsterItem> list;
  num totalCount;

  MonsterListResult(this.msg, this.code, this.list, this.totalCount);

  factory MonsterListResult.fromJson(Map<String, dynamic>json) {
    return MonsterListResult(
      json['msg'],
      json['code'],
      json['list'] != null ? (json['list'] as List).map((e) => MonsterItem.fromJson(e)).toList() : [],
        json['totalCount']
    );
  }
}
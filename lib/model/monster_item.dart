class MonsterItem {
  num id;
  String monsterName;
  String imgUrl;
  num monsterHp;
  num monsterAttack;
  num monsterDefence;
  num monsterSpeed;
  String monsterType;

  MonsterItem(this.id, this.monsterName, this.imgUrl, this.monsterHp, this.monsterAttack, this.monsterDefence, this.monsterSpeed, this.monsterType);

  factory MonsterItem.fromJson(Map<String, dynamic>json) {
    return MonsterItem(
      json['id'],
      json['monsterName'],
      json['imgUrl'],
      json['monsterHp'],
      json['monsterAttack'],
      json['monsterDefence'],
      json['monsterSpeed'],
      json['monsterType']
    );
  }
}